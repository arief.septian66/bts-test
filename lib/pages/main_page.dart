part of 'pages.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  ItemCubit itemCubit = ItemCubit();

  @override
  void initState() {
    UserState userState = BlocProvider.of<UserCubit>(context).state;
    if (userState is UserLogged) {
      itemCubit.loadDataItem(context, userState.user.token);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () async {
          UserState userState = BlocProvider.of<UserCubit>(context).state;
          if (userState is UserLogged) {
            bool? returnResult = await showDialog(
                context: context,
                builder: (context) {
                  return ItemDialog(token: userState.user.token);
                });

            if (returnResult != null) {
              UserState userState = BlocProvider.of<UserCubit>(context).state;
              if (userState is UserLogged) {
                itemCubit.loadDataItem(context, userState.user.token);
              }
            }
          }
        },
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 4,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        centerTitle: false,
        title: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'BTS TEST',
                style: mainFont.copyWith(
                    fontSize: 16,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold),
              ),
              GestureDetector(
                onTap: () {
                  BlocProvider.of<UserCubit>(context).logout(context);
                },
                child: const Icon(
                  Icons.logout,
                  color: Colors.red,
                ),
              )
            ],
          ),
        ),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return BlocBuilder<ItemCubit, ItemState>(
        bloc: itemCubit,
        builder: (context, state) {
          if (state is ItemListLoaded) {
            return state.data.isEmpty
                ? Center(
                    child: Text(
                    'Data Kosong',
                    style:
                        mainFont.copyWith(fontSize: 16, color: Colors.black87),
                  ))
                : ListView.builder(
                    itemCount: state.data.length,
                    itemBuilder: (context, i) {
                      return Container(
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Card(
                              child: Container(
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              children: [
                                Text(state.data[i].name),
                                const SizedBox(height: 16),
                                Row(children: [
                                  Flexible(
                                      flex: 1,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      ItemDetailPage(
                                                          data:
                                                              state.data[i])));
                                        },
                                        child: Container(
                                          width: double.infinity,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              border: Border.all(
                                                  color: Theme.of(context)
                                                      .primaryColor)),
                                          child: Text(
                                            'Detail',
                                            style: mainFont.copyWith(
                                                fontSize: 14,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )),
                                  const SizedBox(width: 5),
                                  Flexible(
                                      flex: 1,
                                      child: GestureDetector(
                                        onTap: () {
                                          UserState userState =
                                              BlocProvider.of<UserCubit>(
                                                      context)
                                                  .state;
                                          if (userState is UserLogged) {
                                            ItemService.deleteItem(context,
                                                    itemId: state.data[i].id
                                                        .toString(),
                                                    token: userState.user.token)
                                                .then((value) {
                                              if (value.status ==
                                                  RequestStatus
                                                      .success_request) {
                                                Fluttertoast.showToast(
                                                    msg:
                                                        'Berhasil Menghapus Item');

                                                itemCubit.loadDataItem(context,
                                                    userState.user.token);
                                              } else {
                                                Fluttertoast.showToast(
                                                    msg: value.data ??
                                                        'Terjadi Kesalahan');
                                              }
                                            });
                                          }
                                        },
                                        child: Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: Colors.red),
                                          alignment: Alignment.center,
                                          child: Text(
                                            'Hapus',
                                            style: mainFont.copyWith(
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )),
                                ])
                              ],
                            ),
                          )));
                    });
          } else if (state is ItemLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ItemFailed) {
            return Text(state.data.returnString ?? 'Terjadi Kesalahan');
          } else {
            return Container();
          }
        });
  }
}

class ItemDialog extends StatefulWidget {
  final String token;
  const ItemDialog({Key? key, required this.token}) : super(key: key);

  @override
  _ItemDialogState createState() => _ItemDialogState();
}

class _ItemDialogState extends State<ItemDialog> {
  TextEditingController controller = TextEditingController();
  bool isPressable = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Item',
                    style: mainFont.copyWith(
                        fontSize: 16,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold)),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  height: 1,
                  width: double.infinity,
                  color: Colors.black12,
                ),
                FormHelper.roundedTextfield(context, controller: controller,
                    onChanged: (value) {
                  if (value.isEmpty) {
                    setState(() {
                      isPressable = false;
                    });
                  } else {
                    setState(() {
                      isPressable = true;
                    });
                  }
                }, maxLines: 1, hintText: 'Data Item'),
                const SizedBox(height: 16),
                Row(children: [
                  Flexible(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Theme.of(context).primaryColor)),
                          child: Text(
                            'Batal',
                            style: mainFont.copyWith(
                                fontSize: 14,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )),
                  const SizedBox(width: 5),
                  Flexible(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          if (isPressable) {
                            EasyLoading.show();
                            ItemService.addItem(context,
                                    itemName: controller.text,
                                    token: widget.token)
                                .then((value) {
                              EasyLoading.dismiss();
                              if (value.status ==
                                  RequestStatus.success_request) {
                                Fluttertoast.showToast(
                                    msg: 'Berhasil Menambahkan Item');
                                Navigator.pop(context, true);
                              } else {
                                Fluttertoast.showToast(
                                    msg: value.data ?? 'Terjadi Kesalahan');
                              }
                            });
                            // AbsenceAPI.absenceIzin(
                            //         data: widget.data,
                            //         token: widget.token,
                            //         keterangan: controller.text)
                            //     .then((value) {
                            //   EasyLoading.dismiss();
                            //   if (value.status ==
                            //       RequestStatus.success_request) {
                            //     Navigator.pop(context, true);
                            //   } else {
                            //     Fluttertoast.showToast(
                            //         msg: 'Gagal Melakukan Absen');
                            //   }
                            // });
                          }
                        },
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: isPressable
                                  ? Theme.of(context).primaryColor
                                  : Colors.grey),
                          alignment: Alignment.center,
                          child: Text(
                            'Tambah',
                            style: mainFont.copyWith(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )),
                ])
              ],
            )));
  }
}

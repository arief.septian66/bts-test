part of 'pages.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();

    startNavigate();
  }

  startNavigate() async {
    var _duration = const Duration(seconds: 2);
    // BlocProvider.of<UserCubit>(context).loadSession();
    return Timer(_duration, () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.getString('login') == null) {
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => const AuthPage()));
      } else {
        BlocProvider.of<UserCubit>(context).loadUser(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: MediaQuery.of(context).size.width * 0.6,
                  child: Image.asset(logoAssetImage))
            ],
          )),
    );
  }
}

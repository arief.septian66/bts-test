part of 'pages.dart';

class ItemDetailPage extends StatefulWidget {
  final Item data;
  const ItemDetailPage({Key? key, required this.data}) : super(key: key);

  @override
  _ItemDetailPageState createState() => _ItemDetailPageState();
}

class _ItemDetailPageState extends State<ItemDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () async {
          // UserState userState = BlocProvider.of<UserCubit>(context).state;
          // if (userState is UserLogged) {
          //   bool? returnResult = await showDialog(
          //       context: context,
          //       builder: (context) {
          //         return ItemDialog(token: userState.user.token);
          //       });

          //   if (returnResult != null) {
          //     UserState userState = BlocProvider.of<UserCubit>(context).state;
          //     if (userState is UserLogged) {
          //       itemCubit.loadDataItem(context, userState.user.token);
          //     }
          //   }
          // }
        },
      ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 4,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        centerTitle: false,
        title: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black87,
                ),
              ),
              const SizedBox(width: 16),
              Text(
                widget.data.name,
                style: mainFont.copyWith(
                    fontSize: 16,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

part of 'pages.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                height: MediaQuery.of(context).size.width * 0.4,
                child: Image.asset(logoAssetImage)),
            SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            Text(
              'MASUK',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: Text(
                'Silahkan Login Atau Daftar Untuk Masuk Kedalam Sistem',
                textAlign: TextAlign.center,
                style: mainFont.copyWith(
                  fontSize: 15,
                  color: Colors.black54,
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: FormHelper.roundedTextfield(context,
                  prefixIcon: Icons.account_circle_rounded,
                  hintText: 'Username',
                  controller: usernameController),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: FormHelper.roundedTextfield(context,
                  prefixIcon: Icons.lock,
                  obscureText: true,
                  suffixWidget: const Icon(Icons.visibility),
                  hintText: 'Password',
                  controller: passwordController),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: FormHelper.roundedFillButton(context, title: 'Masuk',
                    onTap: () {
                  BlocProvider.of<UserCubit>(context).login(
                    context,
                    username: usernameController.text,
                    password: passwordController.text,
                  );
                })),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            RichText(
                text: TextSpan(children: [
              TextSpan(
                  text: 'Tidak Memiliki Akun? ',
                  style:
                      mainFont.copyWith(fontSize: 14, color: Colors.black87)),
              TextSpan(
                  text: 'DAFTAR',
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => const RegisterPage()));
                    },
                  style: mainFont.copyWith(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      color: Theme.of(context).primaryColor)),
            ])),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
          ],
        ),
      ),
    );
  }
}

part of 'pages.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: MediaQuery.of(context).size.width * 0.2),
            const Icon(Icons.arrow_back_ios, color: Colors.black87),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            Text(
              'Daftar',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              'Silahkan Mengisi Form Dibawah Ini\nUntuk Melakukan Pendaftaran',
              style: mainFont.copyWith(
                fontSize: 14,
                color: Colors.black87,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            Text(
              'Email',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.01),
            FormHelper.roundedTextfield(context,
                hintText: 'Email', controller: emailController),
            SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            Text(
              'Username',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.01),
            FormHelper.roundedTextfield(context,
                hintText: 'Username', controller: usernameController),
            SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            Text(
              'Password',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.01),
            FormHelper.roundedTextfield(context,
                obscureText: true,
                hintText: 'Password',
                controller: passwordController),
            SizedBox(height: MediaQuery.of(context).size.width * 0.05),
            Text(
              'Konfirmasi Password',
              style: mainFont.copyWith(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: MediaQuery.of(context).size.width * 0.01),
            FormHelper.roundedTextfield(context,
                hintText: 'Konfirmasi Password',
                obscureText: true,
                controller: confirmPasswordController),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
            FormHelper.roundedFillButton(context, onTap: () {
              UserForm userForm = UserForm(
                  email: emailController.text,
                  password: passwordController.text,
                  username: usernameController.text);
              EasyLoading.show(status: 'Mohon Tunggu...');
              UserService.register(context, userForm: userForm).then((value) {
                EasyLoading.dismiss();
                if (value.status == RequestStatus.success_request) {
                  Fluttertoast.showToast(msg: 'Daftar Berhasil Silahkan Login');
                  Navigator.pop(context);
                } else {
                  Fluttertoast.showToast(msg: value.data!);
                }
              });
            }, title: 'Daftar'),
            SizedBox(height: MediaQuery.of(context).size.width * 0.1),
          ],
        ),
      ),
    );
  }
}

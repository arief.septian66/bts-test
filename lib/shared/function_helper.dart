part of 'shared.dart';

devPrint(Object object) {
  if (developmentStatus) {
    print(object);
  }
}

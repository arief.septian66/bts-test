part of 'models.dart';

class Item {
  late int id;
  late String name;
  late bool checklistCompletionStatus;

  Item.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    name = jsonMap['name'];
    checklistCompletionStatus = jsonMap['checklistCompletionStatus'];
  }
}

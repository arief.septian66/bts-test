part of 'models.dart';

class User {
  late String token;

  User({required this.token});

  User.fromJson(Map<String, dynamic> jsonMap) {
    token = jsonMap['token'];
  }
}

class UserForm {
  late String email;
  late String password;
  late String username;

  UserForm(
      {required this.email, required this.password, required this.username});
}

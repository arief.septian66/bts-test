part of 'services.dart';

class ItemService {
  static Future<ApiReturnValue<String?>> addItem(BuildContext context,
      {required String itemName, required String token}) async {
    ApiReturnValue<String?> apiReturnValue;

    const String url = baseUrl + '/checklist';

    Map<String, String> jsonMap = {};

    jsonMap['name'] = itemName;

    try {
      var request = await http.post(Uri.parse(url),
          headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer $token'
          },
          body: json.encode(jsonMap));

      final returnAPI = json.decode(request.body);

      if (request.statusCode == 200) {
        if (returnAPI['statusCode'] == 2000) {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['message'],
              status: RequestStatus.success_request);
        } else {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['errorMessage'],
              status: RequestStatus.failed_request);
        }
      } else {
        apiReturnValue = ApiReturnValue(
            data: json.decode(request.body)['errorMessage'],
            status: RequestStatus.failed_request);
      }
    } catch (e) {
      print(e);
      apiReturnValue = ApiReturnValue(
          data: 'Terjadi Kesalahan', status: RequestStatus.failed_parsing);
    }

    return apiReturnValue;
  }

  static Future<ApiReturnValue<String?>> deleteItem(BuildContext context,
      {required String itemId, required String token}) async {
    ApiReturnValue<String?> apiReturnValue;

    String url = baseUrl + '/checklist/$itemId';

    try {
      var request = await http.delete(
        Uri.parse(url),
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Bearer $token'
        },
      );

      final returnAPI = json.decode(request.body);

      if (request.statusCode == 200) {
        if (returnAPI['statusCode'] == 2300) {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['message'],
              status: RequestStatus.success_request);
        } else {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['errorMessage'],
              status: RequestStatus.failed_request);
        }
      } else {
        apiReturnValue = ApiReturnValue(
            data: json.decode(request.body)['errorMessage'],
            status: RequestStatus.failed_request);
      }
    } catch (e) {
      print(e);
      apiReturnValue = ApiReturnValue(
          data: 'Terjadi Kesalahan', status: RequestStatus.failed_parsing);
    }

    return apiReturnValue;
  }

  static Future<ApiReturnValue<List<Item>?>> listItem(BuildContext context,
      {required String token}) async {
    ApiReturnValue<List<Item>?> apiReturnValue;

    const String url = baseUrl + '/checklist';

    Map<String, String> jsonMap = {};

    try {
      var request = await http.get(
        Uri.parse(url),
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Bearer $token'
        },
      );

      final returnAPI = json.decode(request.body);

      if (request.statusCode == 200) {
        if (returnAPI['statusCode'] == 2100) {
          List<Item> dataFinal = [];

          for (var i = 0; i < returnAPI['data'].length; i++) {
            dataFinal.add(Item.fromJson(returnAPI['data'][i]));
          }
          apiReturnValue = ApiReturnValue(
              data: dataFinal, status: RequestStatus.success_request);
        } else {
          apiReturnValue = ApiReturnValue(
              data: null,
              returnString: json.decode(request.body)['errorMessage'],
              status: RequestStatus.failed_request);
        }
      } else {
        apiReturnValue = ApiReturnValue(
            data: null,
            returnString: json.decode(request.body)['errorMessage'],
            status: RequestStatus.failed_request);
      }
    } catch (e) {
      print(e);
      apiReturnValue = ApiReturnValue(
          data: null,
          returnString: 'Terjadi Kesalahan',
          status: RequestStatus.failed_parsing);
    }

    return apiReturnValue;
  }
}

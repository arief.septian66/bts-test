part of 'services.dart';

class UserService {
  static Future<ApiReturnValue<String?>> register(BuildContext context,
      {required UserForm userForm}) async {
    ApiReturnValue<String?> apiReturnValue;

    const String url = baseUrl + '/register';

    Map<String, String> jsonMap = {};

    jsonMap['email'] = userForm.email;
    jsonMap['password'] = userForm.password;
    jsonMap['username'] = userForm.username;

    try {
      var request = await http.post(Uri.parse(url),
          headers: {'content-type': 'application/json'},
          body: json.encode(jsonMap));

      final returnAPI = json.decode(request.body);

      if (request.statusCode == 200) {
        if (returnAPI['statusCode'] == 2000) {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['message'],
              status: RequestStatus.success_request);
        } else {
          apiReturnValue = ApiReturnValue(
              data: json.decode(request.body)['errorMessage'],
              status: RequestStatus.failed_request);
        }
      } else {
        apiReturnValue = ApiReturnValue(
            data: json.decode(request.body)['errorMessage'],
            status: RequestStatus.failed_request);
      }
    } catch (e) {
      print(e);
      apiReturnValue = ApiReturnValue(
          data: 'Terjadi Kesalahan', status: RequestStatus.failed_parsing);
    }

    // if (httpRequestData!.status == RequestStatus.success_request) {
    //   if (httpRequestData.data['statusCode']) {
    //     apiReturnValue = ApiReturnValue(
    //         data: httpRequestData.data['message'],
    //         status: RequestStatus.success_request);
    //   } else {
    //     apiReturnValue = ApiReturnValue(
    //         data: httpRequestData.data['message'],
    //         status: RequestStatus.failed_request);
    //   }
    // } else {
    //   apiReturnValue =
    //       ApiReturnValue(data: null, status: httpRequestData.status);
    // }

    return apiReturnValue;
  }

  static Future<ApiReturnValue<User?>> login(BuildContext context,
      {required String username, required String password}) async {
    ApiReturnValue<User?> apiReturnValue;

    const String url = baseUrl + '/login';

    Map<String, String> jsonMap = {};

    jsonMap['username'] = username;
    jsonMap['password'] = password;

    try {
      var request = await http.post(Uri.parse(url),
          headers: {'content-type': 'application/json'},
          body: json.encode(jsonMap));

      final returnAPI = json.decode(request.body);
      print(returnAPI);

      if (request.statusCode == 200) {
        if (returnAPI['statusCode'] == 2110) {
          apiReturnValue = ApiReturnValue(
              data: User.fromJson(returnAPI['data']),
              status: RequestStatus.success_request);
        } else {
          apiReturnValue = ApiReturnValue(
              data: null,
              returnString: json.decode(request.body)['errorMessage'],
              status: RequestStatus.failed_request);
        }
      } else {
        apiReturnValue = ApiReturnValue(
            data: null,
            returnString: json.decode(request.body)['errorMessage'],
            status: RequestStatus.failed_request);
      }
    } catch (e) {
      print(e);
      apiReturnValue = ApiReturnValue(
          data: null,
          returnString: 'Terjadi Kesalahan',
          status: RequestStatus.failed_parsing);
    }

    return apiReturnValue;
  }
}

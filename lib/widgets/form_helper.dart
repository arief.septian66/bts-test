part of 'widgets.dart';

class FormHelper {
  static Widget roundedFillButton(BuildContext context,
      {required String title,
      Widget? prefixWidget,
      double? customTitleSize,
      double? customVerticalPadding,
      Color? colorButton,
      required Function() onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: customVerticalPadding ?? 12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: Border.all(
                width: 1, color: colorButton ?? Theme.of(context).accentColor),
            color: colorButton ?? Theme.of(context).accentColor),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            prefixWidget ?? Container(),
            Text(title,
                style: mainFont.copyWith(
                    fontSize: customTitleSize ?? 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w600)),
          ],
        ),
      ),
    );
  }

  static Widget roundedTextfield(
    BuildContext context, {
    String? hintText,
    IconData? prefixIcon,
    Widget? suffixWidget,
    bool? obscureText,
    String? validation,
    TextInputType? keyboardType,
    String? prefixText,
    TextStyle? prefixStyle,
    EdgeInsetsGeometry? contentPadding,
    int? maxLines,
    List<TextInputFormatter>? inputFormatters,
    Function(String)? onChanged,
    String? Function(String?)? validator,
    bool? enabled,
    int? maxLength,
    Function()? onTap,
    required TextEditingController controller,
  }) {
    return TextFormField(
      controller: controller,
      enabled: enabled ?? true,
      onChanged: onChanged,
      maxLength: maxLength,
      validator: validator,
      onTap: onTap,
      maxLines: maxLines ?? 1,
      keyboardType: keyboardType ?? TextInputType.text,
      obscureText: obscureText ?? false,
      inputFormatters: inputFormatters,
      style: mainFont.copyWith(
        fontSize: 16.0,
      ),
      decoration: InputDecoration(
          hintText: hintText,
          errorText: validation,
          errorStyle: mainFont.copyWith(fontSize: 10.0, color: Colors.red),
          prefixText: prefixText,
          prefixStyle: prefixStyle ??
              mainFont.copyWith(
                  fontSize: 13.0,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
          contentPadding: contentPadding ??
              // ignore: prefer_const_constructors
              EdgeInsets.symmetric(
                vertical: 16,
                horizontal: 16,
              ),
          errorMaxLines: 10,
          hintStyle: mainFont.copyWith(
            fontSize: 14.0,
            color: Colors.black87.withOpacity(0.3),
          ),
          prefixIcon: prefixIcon == null
              ? null
              : Icon(
                  prefixIcon,
                  color: Colors.black87.withOpacity(0.6),
                ),
          suffixIcon: suffixWidget,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                  color: Colors.black87.withOpacity(0.7), width: 1))),
    );
  }
}

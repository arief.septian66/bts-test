part of '../cubits.dart';

class ItemCubit extends Cubit<ItemState> {
  ItemCubit() : super(ItemInitial());

  void loadDataItem(BuildContext context, String token) async {
    emit(ItemLoading());
    ItemService.listItem(context, token: token).then((value) {
      if (value.status == RequestStatus.success_request) {
        emit(ItemListLoaded(value.data!));
      } else {
        emit(ItemFailed(value));
      }
    });
  }
}

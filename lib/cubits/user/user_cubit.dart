part of '../cubits.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());

  void loadUser(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    emit(UserLogged(User(token: prefs.getString('login')!)));
    Navigator.push(
        context, MaterialPageRoute(builder: (_) => const MainPage()));
  }

  void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    Fluttertoast.showToast(msg: 'Logout Berhasil');
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (_) => const AuthPage()), (route) => false);
  }

  void login(
    BuildContext context, {
    required String username,
    required String password,
  }) async {
    try {
      EasyLoading.show(status: 'Mohon Tunggu...');

      UserService.login(
        context,
        username: username,
        password: password,
      ).then((value) async {
        EasyLoading.dismiss();
        if (value.status == RequestStatus.success_request) {
          //create Session
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('login', value.data!.token);
          emit(UserLogged(value.data!));
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => const MainPage()),
              (route) => false);
        } else {
          Fluttertoast.showToast(
              msg: value.returnString ?? 'Terjadi Kesalahan');
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
